const conn = require('../connection')
const Sequelize = require('sequelize')

const genes = conn.define('cancer_genes', {
  cg_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  gene_symbol: { type: Sequelize.STRING }
}, {
  timestamps: false
})

const panels = conn.define('cancer_tests', {
  ct_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  company_name: { type: Sequelize.STRING },
  test_name: { type: Sequelize.STRING },
  cancer_type: { type: Sequelize.STRING }
}, {
  timestamps: false
})

const testGenes = conn.define('cancer_tests_cancer_genes', {
  ctcg_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  ct_id: { type: Sequelize.INTEGER },
  cg_id: { type: Sequelize.INTEGER }
}, {
  timestamps: false
})

module.exports = {
  genes,
  panels,
  testGenes
}
