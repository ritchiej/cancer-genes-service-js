
require('dotenv').config()
const fs = require('fs')
const path = require('path')
const tables = require('./model/tables')
const conn = require('./connection')
const chalk = require('chalk')
const debug = require('debug')('db-loader')

// this could be any of the files in data/companies as they all have the same list of genes
const base = path.join(__dirname, "..", "data", "companies")
const genesFilePath = path.join(base, "Ambry.tsv")
const sql = path.join(__dirname, "..", "cancer-init.sql")


loadDB()

async function loadDB() {
  conn.authenticate().then(() => {
    debug(chalk.green('Connection has been established successfully.'))
  })
    .catch(err => {
      console.err(chalk.red('Unable to connect to the database:'), err, conn.config)
    })


  await createDb()

  const cancerCompaniesDirectory = path.join(__dirname, "..", "data", "companies")
  const files = fs.readdirSync(cancerCompaniesDirectory)
  const lines = fs.readFileSync(genesFilePath, 'utf-8').split('\n')
  await saveGenes(lines)
  for (const f of files) {
    await saveCancerGeneTestInfoByCompany(path.join(base, f))
  }
  console.log("Cancer DB successfully loaded!")
  process.exit()
}

async function createDb() {
  console.log("Creating db")
  const init = fs.readFileSync(sql).toString()
  return conn.query(init)
}

async function saveGenes(lines) {
  for (const line of lines) {
    const split = line.split('\t')
    if (split[0] === '' || split[0] === 'Company' || split[0] === 'Cancer' || split[0] === 'Gene/Test') {
      continue
    }
    await tables.genes.create({
      gene_symbol: split[0].trim()
    })
  }
}

async function saveCancerGeneTestInfoByCompany(file) {
  const lines = fs.readFileSync(file, 'utf-8').split('\n')
  let company = ''
  let testNames = []
  let cancerTypes = []
  for (const line of lines) {
    const split = line.split('\t')

    if (split[0] === 'Company') {
      company = split[1]
    } else if (split[0] === 'Cancer') {
      for (const s of split) {
        if (s !== 'Cancer') {
          cancerTypes.push(s.trim())
        }
      }
    } else if (split[0] === 'Gene/Test') {
      let count = 0
      for (const s of split) {
        if (s !== 'Gene/Test') {
          testNames.push(s.trim())
          await tables.panels.create(
            {
              company_name: company,
              test_name: s.trim(),
              cancer_type: cancerTypes[count]
            })
          count++
        }
      }
    } else {
      let count = 0
      const cgId = await tables.genes.find({
        attributes: ['cg_id'],
        where: { gene_symbol: split[0].trim() }
      })
      for (const s of split) {
        if (s !== split[0]) {
          if (s !== '') {
            const ctId = await tables.panels.find({
              attributes: ['ct_id'],
              where: {
                company_name: company,
                test_name: testNames[count]
              }
            })
            await tables.testGenes.create({
              ct_id: ctId.dataValues.ct_id,
              cg_id: cgId.dataValues.cg_id
            })
          }
          count++
        }
      }
    }
  }
}
