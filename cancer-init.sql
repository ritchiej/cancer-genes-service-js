DROP TABLE IF EXISTS cancer_genes CASCADE;
DROP TABLE IF EXISTS cancer_tests CASCADE;
DROP TABLE IF EXISTS cancer_tests_cancer_genes CASCADE;

CREATE TABLE cancer_tests (
  ct_id SERIAL PRIMARY KEY,
  company_name VARCHAR(255) NOT NULL,
  test_name VARCHAR(255) NOT NULL,
  cancer_type VARCHAR(255) NOT NULL
);

CREATE TABLE cancer_tests_cancer_genes (
  ctcg_id SERIAL PRIMARY KEY,
  ct_id INTEGER NOT NULL,
  cg_id INTEGER NOT NULL
);

CREATE TABLE cancer_genes (
  cg_id SERIAL PRIMARY KEY,
  gene_symbol VARCHAR(255) NOT NULL
);
