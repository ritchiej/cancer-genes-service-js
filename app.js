
require('dotenv').config()
const express = require('express')
const chalk = require('chalk')
const debug = require('debug')('app')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')
const conn = require('./db/connection')
const homeRoutes = require('./routes/home-routes')
const geneRoutes = require('./routes/gene-routes')
const panelRoutes = require('./routes/panel-routes')
const cancerRoutes = require('./routes/cancer-routes')

const app = express()
const port = process.env.PORT || 3000

app.use(morgan('tiny'))

conn
  .authenticate()
  .then(() => {
    debug(chalk.green('Connection has been established successfully.'))
  })
  .catch(err => {
    console.error(chalk.red('Unable to connect to the database:'), err)
  })

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.get('/', homeRoutes.home)

app.get('/genes', geneRoutes.genes)
app.get('/genes/cancer/:cancer', geneRoutes.genesByType)
app.get('/genes/panel/:panel', geneRoutes.genesByPanel)

app.get('/cancers/gene/:gene', cancerRoutes.typesByGene)
app.get('/cancers', cancerRoutes.cancers)
app.post('/cancers', cancerRoutes.typesByGenes)

app.get('/panels', panelRoutes.panels)
app.get('/panels/gene/:gene', panelRoutes.panelsByGene)

if (process.env.NODE_ENV === 'development') {
  swaggerDocument.host = `${process.env.SWAGGER_HOST}:${port}`
}

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.listen(port, () => {
  debug(`listening on port ${chalk.green(port)}`)
})
