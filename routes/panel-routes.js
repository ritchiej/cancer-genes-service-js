
const tables = require('../db/model/tables')
const conn = require('../db/connection')

async function panels (req, res) {
    const tests = await tables.panels.findAll()
    const panels = tests.map(t => t.test_name)
    res.json(panels)
}

async function panelsByGene (req, res) {
    console.log(req.params.gene)
    const panels = await conn.query(`SELECT company_name, test_name
        FROM cancer_tests ct
        JOIN cancer_tests_cancer_genes ctcg ON ct.ct_id = ctcg.ct_id
        JOIN cancer_genes cg ON cg.cg_id = ctcg.cg_id
        WHERE cg.gene_symbol = :gene; `, {
            replacements: { gene: req.params.gene.toUpperCase() },
            type: conn.QueryTypes.SELECT
    })
    res.json(panels)
}

module.exports = {
  panels,
  panelsByGene
}
