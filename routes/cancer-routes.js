
const tables = require('../db/model/tables')
const conn = require('../db/connection')

async function cancers (req, res) {
    const cancers = await conn.query(`SELECT DISTINCT(ct.cancer_type) FROM cancer_tests ct;`,{
        type: conn.QueryTypes.SELECT
    })
    const cancerTypes = cancers.map(c => c.cancer_type)
    res.json(cancerTypes);
}

async function typesByGene (req, res) {
    console.log(req.params.gene)
    const types = await conn.query(`SELECT DISTINCT(ct.cancer_type)
        FROM cancer_tests ct
        JOIN cancer_tests_cancer_genes ctcg ON ct.ct_id = ctcg.ct_id
        JOIN cancer_genes cg ON cg.cg_id = ctcg.cg_id
        WHERE cg.gene_symbol = :gene; `, {
            replacements: { gene: req.params.gene.toUpperCase() },
            type: conn.QueryTypes.SELECT
    })
    const cancertypes = types.map(ct => ct.cancer_type)
    res.json(cancertypes)
}

async function typesByGenes (req, res) {
    const types = await conn.query(`SELECT DISTINCT(ct.cancer_type)
        FROM cancer_tests ct
        JOIN cancer_tests_cancer_genes ctcg ON ct.ct_id = ctcg.ct_id
        JOIN cancer_genes cg ON cg.cg_id = ctcg.cg_id
        WHERE cg.gene_symbol IN (:gene); `, {
            replacements: { gene: req.body.genes.map(g => g.toUpperCase()) },
            type: conn.QueryTypes.SELECT
    })
    const cancertypes = types.map(ct => ct.cancer_type)
    res.json(cancertypes)
}

module.exports = {
  cancers,
  typesByGene,
  typesByGenes
}
