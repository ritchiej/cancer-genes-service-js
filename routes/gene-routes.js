
const tables = require('../db/model/tables')
const conn = require('../db/connection')

async function genes (req, res) {
    const genes = await tables.genes.findAll()
    const gene_symbols = genes.map(g => g.gene_symbol)
    res.json(gene_symbols)
}

async function genesByType (req, res) {
    console.log(req.params.cancer)
    const genes = await conn.query(`SELECT DISTINCT(cg.gene_symbol)
        FROM cancer_genes cg
        JOIN cancer_tests_cancer_genes ctcg ON cg.cg_id = ctcg.cg_id
        JOIN cancer_tests ct ON ct.ct_id = ctcg.ct_id
        WHERE cancer_type LIKE :cancer; `, {
            replacements: { cancer: req.params.cancer },
            type: conn.QueryTypes.SELECT
    })
    const cancerGenes = genes.map(g => g.gene_symbol)
    res.json(cancerGenes)
}

async function genesByPanel (req, res) {
    console.log(req.params.panel)
    const genes = await conn.query(`SELECT DISTINCT(cg.gene_symbol)
    FROM cancer_genes cg
    JOIN cancer_tests_cancer_genes ctcg ON cg.cg_id = ctcg.cg_id
    JOIN cancer_tests ct ON ct.ct_id = ctcg.ct_id
    WHERE test_name = :panel;`, {
        replacements: { panel: req.params.panel },
        type: conn.QueryTypes.SELECT
    })
    const cancerGenes = genes.map(g => g.gene_symbol)
    res.json(cancerGenes)
}

module.exports = {
  genes,
  genesByType,
  genesByPanel
}
